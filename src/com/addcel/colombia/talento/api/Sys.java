package com.addcel.colombia.talento.api;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class Sys {

	public static String IMEI = null;

	public static String getIMEI(Context ctx) {
		if (IMEI == null) {

			try {
				IMEI = ((TelephonyManager) ctx
						.getSystemService(Context.TELEPHONY_SERVICE))
						.getDeviceId();

				if (IMEI == null) {
					IMEI = Settings.System.getString(ctx.getContentResolver(),
							Settings.System.ANDROID_ID);
				}
			} catch (Exception e) {
				IMEI = Settings.System.getString(ctx.getContentResolver(),
						Settings.System.ANDROID_ID);
			}
		}
		return IMEI;
	}

	public static String getTipo() {
		return Build.MANUFACTURER;
	}

	public static String getSWVersion() {
		return Build.VERSION.RELEASE;
	}

	public static String getModel() {
		return Build.MODEL;
	}

}
