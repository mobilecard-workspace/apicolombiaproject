package com.addcel.colombia.talento.api;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class Action {

	private String nameSpace = "urn:gatewayctt";
	private String url = "http://186.28.237.67/rcn_ctt/datos.php";
	private String methodName = "votacionctt";
	private String soapAction = "urn:gatewayctt#votacionctt";
	
	private SoapObject request = null;
	private SoapSerializationEnvelope envelope = null;
	private SoapPrimitive resultsRequestSOAP = null;
	
	public String doAction(Consumer consumer) {
		
		String result = null;
		
		try {

			request = new SoapObject(nameSpace, methodName);

			PropertyInfo cliente = new PropertyInfo();
			cliente.setName("cliente");
			//cliente.setValue("addcel");
			cliente.setValue(consumer.getUser());
			cliente.setType(String.class);

			PropertyInfo pass = new PropertyInfo();
			pass.setName("pass");
			//pass.setValue("Cun85583Z2");
			pass.setValue(consumer.getPassword());
			pass.setType(String.class);

			request.addProperty(cliente);
			request.addProperty(pass);

			envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = false;
			envelope.setOutputSoapObject(request);
			HttpTransportSE transporte = new HttpTransportSE(url);
			transporte.call(soapAction, envelope);
			result = (String) envelope.getResponse();

		} catch (Exception e) {
			e.printStackTrace();
			result = "{\"error\":1,\"descripcion\":" + e.toString() + "\"}";
		}
		
		return result;
	}
}

