package com.addcel.colombia.talento.api;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import com.addcel.colombia.talento.api.Action;
import com.addcel.colombia.talento.api.Consumer;

import android.util.Log;

public class AndroidPlugin extends CordovaPlugin {
	public static final String NATIVE_ACTION_STRING="nativeAction"; 
    public static final String SUCCESS_PARAMETER="success"; 

    
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    	
        Log.d("AndroidPlugin", "Hello, this is a native function called from PhoneGap/Cordova!"); 

        
        String resultType = args.getString(0);
        
        if ("interactUI".equals(resultType)) {
        	//callbackContext.success("return true");
        	interactUI(callbackContext, args);
        	return true;
        } else if ("interact".equals(resultType)){
        	interact(callbackContext, args);
        	return true;
        }

        return false;
    }
    
    
    private boolean interactUI(final CallbackContext callbackContext, JSONArray args) throws JSONException{
        final String duration = args.getString(0);
        
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Consumer consumer = new Consumer("addcel", "Cun85583Z2");
                Action action = new Action();
                String json = action.doAction(consumer);
                callbackContext.success(json); // Thread-safe.
            }
        });
        return true;
    }
    

    
    private boolean interact(final CallbackContext callbackContext, JSONArray args) throws JSONException{
        
    	final String duration = args.getString(0);
        
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {

                Consumer consumer = new Consumer("addcel", "Cun85583Z2");
                Action action = new Action();
                String json = action.doAction(consumer);
            	
                callbackContext.success(json); // Thread-safe.
            }
        });
        return true;
    }
    
    
} 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    public PluginResult execute(String action, JSONArray data, String callbackId) { 
           Log.d("HelloPlugin", "Hello, this is a native function called from PhoneGap/Cordova!"); 
           //only perform the action if it is the one that should be invoked 
           if (NATIVE_ACTION_STRING.equals(action)) {
                 String resultType = null; 
                 try {
                       //resultType = data.getString(0);
                	 resultType = SUCCESS_PARAMETER;
                 } 
                 catch (Exception ex) { 
                       Log.d("HelloPlugin", ex.toString()); 
                 } 
                 if (resultType.equals(SUCCESS_PARAMETER)) { 
                       return new PluginResult(PluginResult.Status.OK, "Yay, Success!!!"); 
                 } 
                 else { 
                       return new PluginResult(PluginResult.Status.ERROR, "Oops, Error :("); 
                 } 
           } 
           return null; 
    } 
    */
